## warm-flow

此项目是极其简单的工作流，没有太多设计，代码量少，并且只有6张表，一个小时就可以看完整个设计。使用起来方便

1. 支持简单的流程流转，比如跳转、回退、审批
2. 支持角色、部门和用户等权限配置
3. 官方提供简单流程封装很实用
4. 支持多租户，感谢【luoheyu】PR
5. 支持代办任务和已办任务，通过权限标识过滤数据
6. 支持互斥网关，并行网关（会签、或签）
7. 可退回任意节点
8. 支持条件表达式

具体demo项目：[hh-vue](https://gitee.com/min290/hh-vue)

更新记录 ：https://gitee.com/warm_4/warm-flow/wikis/Home

流程规则 ：https://gitee.com/warm_4/warm-flow/wikis/%E6%B5%81%E7%A8%8B%E8%A7%84%E5%88%99

常见问题 ：https://gitee.com/warm_4/warm-flow/wikis/%E5%B8%B8%E8%A7%81%E9%97%AE%E9%A2%98

## qq群：778470567  微信:warm-houhou

## 演示地址
- admin/admin123

演示地址：http://www.hhzai.top:81


## 演示图
<table>
    <tr>
        <td><img src="https://foruda.gitee.com/images/1697704379975758657/558474f6_2218307.png"/></td>
        <td><img src="https://foruda.gitee.com/images/1697704511309847220/40ecb5a0_2218307.png"/></td>
    </tr>
    <tr>
        <td><img src="https://foruda.gitee.com/images/1697704522482465368/a161abaf_2218307.png"/></td>
        <td><img src="https://foruda.gitee.com/images/1697704361102991586/f831523d_2218307.png"/></td>
    </tr>
    <tr>
        <td><img src="https://foruda.gitee.com/images/1697704550238134213/8abf2e7e_2218307.png"/></td>
        <td><img src="https://foruda.gitee.com/images/1697704582500705717/a87d0c32_2218307.png"/></td>
    </tr>
    <tr>
        <td><img src="https://foruda.gitee.com/images/1697767718130579881/8f29cbfc_2218307.png"/></td>
        <td><img src="https://foruda.gitee.com/images/1697767731619319111/59871f55_2218307.png"/></td>
    </tr>
    <tr>
        <td><img src="https://foruda.gitee.com/images/1698042602148217599/0cdd247e_2218307.png"/></td>
        <td><img src="https://foruda.gitee.com/images/1698042624784729710/7c85ec5a_2218307.png"/></td>
    </tr>
</table>

## 你可以请作者喝杯咖啡表示鼓励

![输入图片说明](https://foruda.gitee.com/images/1697770422557390406/7efa04d6_2218307.png "屏幕截图")


git提交规范

    [init] 初始化  
    [feat] 增加新功能  
    [fix] 修复问题/BUG  
    [style] 代码风格相关无影响运行结果的  
    [perf] 优化/性能提升  
    [refactor] 重构  
    [revert] 撤销修改  
    [test] 测试相关  
    [docs] 文档/注释  
    [chore] 依赖更新/脚手架配置修改等  
    [workflow] 工作流改进  
    [ci] 持续集成  
    [types] 类型定义文件更改  
    [wip] 开发中
